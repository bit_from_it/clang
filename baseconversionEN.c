/* Program baseconversion.c
** 
** Purpose: converts a given number of base b
** to all numbers of base 2 to  16.
**
** Compile: gcc baseconversionEN.c -lm -o basecon
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h> //for the tolower() function
#define NMAX 100

// Function to control if the number inserted is valid
int validnumber(int b, char *p){
	char base_digits[16] = {'0', '1', '2', '3', '4', '5', '6', '7',
	  			'8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
	int len = strlen(p) - 1, index;
	int sum = 0.;	

	//Is the base valid?
	if (b < 2 || b > 16){ 
		printf("Hm, the base can't be less than 2 or larger than 16! The program will terminate!\n");
		return 0;
	}
	// Here I compare the ASCII value of every character of my string with the maximum digit of a given base b.
	for(index = 0; index < len; index++){
		if ((len > 0) && ((int)p[0] == (int)'-')){
			printf("ERROR! You have inserted a negative number. The program will terminate.\n");
			return 0;
		}
		else if ((len == 1) && ((int)p[0] == (int)'0')){
			printf("ACHTUNG! You've inserted zero. Try again with a nonzero value.\n");
			return 0;
		}
		else if ((((int)p[index] <= (int)base_digits[b-1]) && 
				((int)p[index] >= (int)'0')) || 
				(((int)p[index] <= (int)putchar(tolower(base_digits[b-1]))) && //checking for small letters
				((int)p[index] >= (int)'0'))){ 
				sum += (int)p[index]; //calculating the sum of the ASCII value of every digit
				continue;
		}
		else{
			printf("\nI'm sorry, you gave an invalid number or base. The program will terminate.\n");
			return 0;
		}				
	}

	// Checking the case where someone inserts only zeros, ex. 0000000
	if (sum == len*(int)'0'){
		printf("Inserted only zeros !!! The program will terminate.\n");
		return 0;
	}
	return 1;
}

// Function to convert the number from base b to base 10 (decimals).
unsigned long base2dec(int b, char *p){
	int index, len = strlen(p) - 1, number, dig, y; 
	unsigned long int sum;
	char s;

	for(index = 0; index < len; index++){
		s = p[index];
		if ((s >= '0') && (s <= '9')){
		    dig = s - '0';
		}
		else if ((s >= 'a') && (s <= 'f')){
		    dig = s - 'a' + 10;
		}
		else if ((s >= 'A') && (s <= 'F')){
		    dig = s - 'A' + 10;
		}

		y = len - index - 1;
		sum += dig*pow(b, y); // Calculating the sum of the product of the last digit of the number with the base raised to the index
	}
	return sum;
}

// Function to convert the number from base 10 to a given base b.
void dec2base(int b, unsigned long x){
	char base_digits[16] = {'0', '1', '2', '3', '4', '5', '6', '7',
	  			'8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

	int converted_number[64];
	int next_digit, index = 0;
	// Conversion to base b
   	while (x > 0){
	 	converted_number[index] = x % b;
	 	x /= b;
	 	++index;
   	}
	// Printing the result
   	--index;
   	printf("Converted Number = ");
   	for(; index >= 0; index--){ // Reading the list from the end to the beginning
	 	printf("%c", base_digits[converted_number[index]]);
   	}
   	printf("\n");
}


// The main function
void main(void){
	//	long int number_to_convert;
	int base, index, len;	
	char snumber[NMAX], c;
	unsigned long decimal;

	// Insert the number and its base.
	printf("ACHTUNG! The program will give correct results for numbers less or equal than 9999999999999998d:\n");
	printf("Insert your number: ");
	fgets(snumber, sizeof(snumber), stdin);
   	printf("Insert a base: ");
   	scanf("%i", &base);	

	// Checking if the number and the base given are valid.
	printf(". \n");
	printf(".. \n");
	printf("... \n");
	printf(".... \n");
	if (validnumber(base,snumber) == 0){
		return;	
	}

	// Conversion of the number given to the decimals.
	decimal = base2dec(base,snumber);

	// Conversion of the decimal equivalent of our number to the rest of the bases.
	for (index = 2; index < 17; index++){
		if (index == 2){
			printf("\n");
		}
		printf(" base = %i, ", index);
		dec2base(index, decimal);
	}
	return;	
}
