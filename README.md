## Synopsis

1. The file baseconversionEN.c converts any number given from any base (2-16) to all the other bases from 2 to 16. 
2. The files approximationEN.c and data.h apply the method of exponential smoothing to given data, calculate important error measures and make a prediction for the next year.

## How to compile them

1. gcc baseconversionEN.c -lm -o basecon
2. gcc approximationsEN.c -lm -I. 
