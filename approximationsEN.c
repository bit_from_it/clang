/***
** Exponential smoothing method.
**
** Compile: gcc approximationsEN.c -lm -I.
** The above mentioned compile was done on Lubuntu GNU/Linux. After I the address where the header file lies must be inserted. 
***/

#include <stdio.h>
#include "data.h"

//------------------------------------------------------------------------------

void main(){
	float alpha, prediction[NMAX];

	printf("Please insert the number α: ");
	scanf("%f",&alpha);

	while(alpha < 0 || alpha > 1){
		printf("ERROR! Alpha must satisfy: 0 <= α <= 1.\n");
		printf("Try again.\n");
		scanf("%f",&alpha);
	}
	//printf("%f \n",alpha);
	
	// Calculations of predictions
	exponential_smoothing(prediction,NMAX,alpha);

	//Printing results
	printf("\n");
	printf("Calculating for smoothing factor α: %2.1f\n",alpha);
	display_error_analysis(prediction, NMAX);
}

//-----------------------------------------------------------------------------------
void exponential_smoothing(float f[], int size, float a){
	// F[t] = F[t-1] + alpha*(DEMAND[t-1] - F[t-1])
	int index;

	f[0] = DEMAND[0];
	//printf("%f, %f \n",f[0],DEMAND[0]);
	//printf("%d\n",size);
	for (index = 1; index < size; index++){
		f[index] = f[index-1] + a*(DEMAND[index-1] - f[index-1]);
		//printf("%f, %f \n",f[index],DEMAND[index]);				
	}	
}

void display_error_analysis(float f[], int size){
	int firstYear = 2005, index;
	float pred15, error, abserror, sqerror, mae = 0, mse = 0;
	
	printf("-----------------------------------------------\n");
	printf("t Year Demand Forecast Error AbsoluteE SquaredE\n");
	printf("-----------------------------------------------\n");
	for (index = 0; index < size - 1; index++){ //size - 1 because I need only the periods from 2005 to 2014 for which I have data for the demand.
	// Calculation of the error
		error = DEMAND[index] - f[index];
	// Absolute error
		abserror = fabs(error);
	// Square error
		sqerror = pow(error,2);
	// Mean absolute error
		mae += abserror/(size-1);
	// Mean square error
		mse += sqerror/(size-1);
	//  Print results for every year
		printf("%2d %4d %4.1f %6.2f %6.2f %5.2f %7.2f\n",index + 1, firstYear, DEMAND[index] ,f[index] ,error ,abserror ,sqerror);
		firstYear++;	
	}
	printf("------------------------------------------------\n");
	printf("\tMAE = %5.2f\n\tMSE = %7.2f\n\t2015 forecast: %6.2f\n",mae, mse,f[10]);
}
